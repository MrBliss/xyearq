package nl.quintor.mb;

import java.util.HashMap;

/**
 * Created by mblijleven on 9/14/15.
 */
public class HistoryOfQuintor {
    public String[] places = {"Groningen", "A\'dam", "Amersfoort", "Den Haag"};
    public String[] skills = {"Java", "Scrum", ".Net", "Analysis"};

    public HistoryOfQuintor() {

    }

    public void print() {
        System.out.println(" ___ ___ ___ ___           ___ ___ ___   ___ \n" +
                "|_  |   |   |  _|   ___   |_  |   |_  | |  _|\n" +
                "|  _| | | | |_  |  |___|  |  _| | |_| |_|_  |\n" +
                "|___|___|___|___|         |___|___|_____|___|\n" +
                "                                            ");
        System.out.println("     ___        _       _");
        System.out.println("    / _ \\ _   _(_)_ __ | |_ ___  _ __");
        System.out.println("   | | | | | | | | '_ \\| __/ _ \\| '__|");
        System.out.println("   | |_| | |_| | | | | | || (_) | |");
        System.out.println("    \\__\\_\\\\__,_|_|_| |_|\\__\\___/|_|");


        System.out.println("");
        System.out.println("          " + places[0] + "        " + skills[0] + "  " + skills[1]);
        System.out.println("              |              |     |");
        System.out.println("              |\\             |     |");
        System.out.println("              | \\            |     |");
        System.out.println("              |  " + places[1] + "       |     |");
        System.out.println("              |    |         |     |");
        System.out.println("              |    |         |     |");
        System.out.println("              |    |         |     |     " + skills[2]);
        System.out.println("              |    x         |     |       |");
        System.out.println("              |              |     |       |");
        System.out.println("              |\\             |     |       |");
        System.out.println("              | \\            |     |       |");
        System.out.println("              |  \\           |     |       |");
        System.out.println("              | " + places[2] + "   |     |       |");
        System.out.println("              |    |         |     |       |");
        System.out.println("              |    |         |     |       |   " + skills[3]);
        System.out.println("              |    |         |     |       |       |");
        System.out.println("              |    |         |     |       |       |");
        System.out.println("             /|    |         |     |       |       |");
        System.out.println("            / |    |         |     |       |       |");
        System.out.println("           /  |    |         |     |       |       |");
        System.out.println("    " + places[3] + "  |    |         |     |       |       |");
        System.out.println("         |    |    |         |     |       |       |");

//        System.out.println("");
//        System.out.println("");
//        System.out.println("");
//        System.out.println("222222222222222         000000000       1111111   555555555555555555");
//        System.out.println("2:::::::::::::::22     00:::::::::00    1::::::1   5::::::::::::::::5");
//        System.out.println("2::::::222222:::::2  00:::::::::::::00 1:::::::1   5::::::::::::::::5");
//        System.out.println("2222222     2:::::2 0:::::::000:::::::0111:::::1   5:::::555555555555");
//        System.out.println("            2:::::2 0::::::0   0::::::0   1::::1   5:::::5");
//        System.out.println("            2:::::2 0:::::0     0:::::0   1::::1   5:::::5");
//        System.out.println("         2222::::2  0:::::0     0:::::0   1::::1   5:::::5555555555");
//        System.out.println("    22222::::::22   0:::::0 000 0:::::0   1::::l   5:::::::::::::::5");
//        System.out.println("  22::::::::222     0:::::0 000 0:::::0   1::::l   555555555555:::::5");
//        System.out.println(" 2:::::22222        0:::::0     0:::::0   1::::l               5:::::5");
//        System.out.println("2:::::2             0:::::0     0:::::0   1::::l               5:::::5");
//        System.out.println("2:::::2             0::::::0   0::::::0   1::::l   5555555     5:::::5");
//        System.out.println("2:::::2       2222220:::::::000:::::::0111::::::1115::::::55555::::::5");
//        System.out.println("2::::::2222222:::::2 00:::::::::::::00 1::::::::::1 55:::::::::::::55");
//        System.out.println("2::::::::::::::::::2   00:::::::::00   1::::::::::1   55:::::::::55");
//        System.out.println("22222222222222222222     000000000     111111111111     555555555");


        //        for (int i =0 ; i < events.length; i++) {
//            System.out.println(events[i]);1
//        }
//        System.out.println("Hello World!");
    }

}

