    package nl.quintor.mb;

    /**
     * Author: mblijleven
     * File XYearsQuintor.java
     * 10 jaar Quintor
     *
     * to run place file XYearsQuintor.java in directory: nl/quintor/mb/
     * paste the content of this file in XYearsQuintor.java
     *
     * From the commandline execute:
     *
     * $> javac nl/quintor/mb/XYearsQuintor.java
     * $> java nl.quintor.mb.XYearsQuintor
     */
    public class XYearsQuintor {
        private static String[] places = {"Groningen", "A\'dam", "Amersfoort", "Den Haag"};
        private static  String[] skills = {"Java", "Scrum", ".Net", "Analysis"};

        public static void main(String[] args) {
            print();
        }

        private static void print() {
            System.out.println(" ___ ___ ___ ___           ___ ___ ___   ___ \n|_  |   |   |  _|   ___   |_  |   |_  | |  _|\n|  _| | | | |_  |  |___|  |  _| | |_| |_|_  |\n|___|___|___|___|         |___|___|_____|___|\n                                            ");
            System.out.println("     ___        _       _\n    / _ \\ _   _(_)_ __ | |_ ___  _ __\n   | | | | | | | | '_ \\| __/ _ \\| '__|\n   | |_| | |_| | | | | | || (_) | |\n    \\__\\_\\\\__,_|_|_| |_|\\__\\___/|_|");
            System.out.println("\n          " + places[0] + "        " + skills[0] + "  " + skills[1] + "\n              |              |     |\n              |\\             |     |\n              | \\            |     |\n              |  " + places[1] + "       |     |\n              |    |         |     |\n              |    |         |     |\n              |    |         |     |     " + skills[2] + "\n              |    x         |     |       |\n              |              |     |       |\n              |\\             |     |       |\n              | \\            |     |       |\n              |  \\           |     |       |\n              | " + places[2] + "   |     |       |\n              |    |         |     |       |\n              |    |         |     |       |   " + skills[3] + "\n              |    |         |     |       |       |\n              |    |         |     |       |       |\n             /|    |         |     |       |       |\n            / |    |         |     |       |       |\n           /  |    |         |     |       |       |\n    " + places[3] + "  |    |         |     |       |       |\n         |    |    |         |     |       |       |\n\n");
            System.out.println(" \\ \\   / /_ _ _ __   | |__   __ _ _ __| |_ ___          \n  \\ \\ / / _` | '_ \\  | '_ \\ / _` | '__| __/ _ \\         \n   \\ V / (_| | | | | | | | | (_| | |  | ||  __/         \n    \\_/ \\__,_|_| |_| |_| |_|\\__,_|_|   \\__\\___|         \n              __      _ _      _ _                    _ \n   __ _  ___ / _| ___| (_) ___(_) |_ ___  ___ _ __ __| |\n  / _` |/ _ \\ |_ / _ \\ | |/ __| | __/ _ \\/ _ \\ '__/ _` |\n | (_| |  __/  _|  __/ | | (__| | ||  __/  __/ | | (_| |\n  \\__, |\\___|_|  \\___|_|_|\\___|_|\\__\\___|\\___|_|  \\__,_|\n  |___/                                                 ");
        }
    }
